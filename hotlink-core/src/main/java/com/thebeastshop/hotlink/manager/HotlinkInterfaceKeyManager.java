package com.thebeastshop.hotlink.manager;

import cn.hutool.core.collection.ConcurrentHashSet;

public class HotlinkInterfaceKeyManager {

    public static final ConcurrentHashSet<String> interfaceKeySet = new ConcurrentHashSet<>();

    public static void addKey(String interfaceKey){
        interfaceKeySet.add(interfaceKey);
    }

    public static boolean containsKey(String interfaceKey){
        return interfaceKeySet.contains(interfaceKey);
    }
}
