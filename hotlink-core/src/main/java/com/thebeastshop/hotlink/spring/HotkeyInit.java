package com.thebeastshop.hotlink.spring;

import com.jd.platform.hotkey.client.ClientStarter;
import org.springframework.beans.factory.InitializingBean;

public class HotkeyInit implements InitializingBean {

    private HotlinkConfig hotlinkConfig;

    public HotkeyInit(HotlinkConfig hotlinkConfig) {
        this.hotlinkConfig = hotlinkConfig;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ClientStarter.Builder builder = new ClientStarter.Builder();
        ClientStarter starter = builder.setAppName(hotlinkConfig.getAppName()).setEtcdServer(hotlinkConfig.getEtcdUrl())
                .setPushPeriod(1000L)
                .build();
        starter.startPipeline();
    }

    public HotlinkConfig getHotlinkConfig() {
        return hotlinkConfig;
    }

    public void setHotlinkConfig(HotlinkConfig hotlinkConfig) {
        this.hotlinkConfig = hotlinkConfig;
    }
}
