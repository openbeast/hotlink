package com.thebeastshop.hotlink.springboot.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "hotlink", ignoreUnknownFields = true)
public class HotlinkProperty {

    private String appName;

    private String etcdUrl;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getEtcdUrl() {
        return etcdUrl;
    }

    public void setEtcdUrl(String etcdUrl) {
        this.etcdUrl = etcdUrl;
    }
}
