package com.thebeastshop.hotlink.test.service;

import com.thebeastshop.hotlink.annotation.Hotlink;

public interface DemoService {

	@Hotlink
	String sayHello1(String name);

	String sayHello2(String name);
}
