## Hotlink客户端框架简介

Hotlink框架是一个基于Hotkey框架的客户端增强版实现，该客户端框架能让Hotkey更完美的落地，增强了Hotkey客户端的能力。

Hotkey是一个京东App后台中间件，毫秒级探测热点数据的解决方案框架

> Hotkey框架项目地址：
> 
> https://gitee.com/jd-platform-opensource/hotkey

该客户端框架有以下特点：

- 业务接入简单，只需要一个标注，1分钟就能使你的RPC接口接入热点探测框架
- 启动时动态扫描所有Hotlink标注的接口，创建动态代理
- 基于动态代理去对接口做增强，理论上只要有接口，就支持任何RPC框架
- 本地方法只要有接口，也能使用热点探测

## Hotlink在Hotkey解决方案中的位置

![2](doc/images/2.png)

## 如何使用Hotlink客户端框架

**第一步**

按照Hotkey的部署要求，搭建好worker和dashboard。具体方式请参照：

> https://gitee.com/jd-platform-opensource/hotkey

同时为了方便大家搭建，我把编译好的worker和dashboard包也进行了上传

worker下载地址：

> 公网IP版本(适合调试用，本地能连上worker)：
>
> https://gitee.com/openbeast/hotlink/attach_files/813746/download/worker-0.0.4-SNAPSHOT-public.jar
>
> 内网IP版本：
>
> https://gitee.com/openbeast/hotlink/attach_files/813747/download/worker-0.0.4-SNAPSHOT.jar
>
> Dashboard:
>
> https://gitee.com/openbeast/hotlink/attach_files/813749/download/dashboard-0.0.2-SNAPSHOT.jar



**第二步**

本地业务项目依赖jar包(此jar包并未上传到中央仓库，需要大家自己deploy到自己公司的私库)

```xml
<dependency>
  <groupId>com.thebeastshop</groupId>
  <artifactId>hotlink-spring-boot-starter</artifactId>
  <version>1.0.12</version>
</dependency>
```

hotlink需要的fastjson和groovy版本有点要求，如果你项目中的这2个包版本过低又同时覆盖了hotlink的传递依赖包时，需要额外指定版本：

```xml
<fastjson.version>1.2.70</fastjson.version>
<guava.version>29.0-jre</guava.version>
```



**第三步**

本地springboot配置文件里加入参数

```properties
#此app-name不配置的话，会优先读取spring.application.name属性
hotlink.app-name=test
#etcd地址和端口
hotlink.etcd-url=http://xxx.xxx.xxx.xxx:2379
```

**第四步**

在你的接口里加入标签`@Hotlink`

在接口上加：接口里所有的方法都会自动探测热点

在方法上加：只有这个方法会自动探测热点

比如：

```java
public interface ProductService{
  @Hotlink
  SkuInfo getSkuInfo(String skuCode);
}
```

那么当某一个SKU001成为热点时，那么传入参数SKU001会自动代理从JVM里取到数据，而SKU002则继续走RPC调用。



这样就完成了所有的配置。启动皆可。



## 使用Hotlink需要注意的事项

由于Hotlink的实现是用动态代理来实现，只要满足这两个条件，即可在启动时会扫描器扫到：

- 接口层面上标注`@Hotlink`
- 相关实现会被注入Spring上下文中



在标注接口的时候，尽量标注在一定时间范围内是幂等的接口。比如会员查询，sku信息查询，相关活动信息的查询，这些信息在一定时间范围内不会频繁变动，那么就适合做热点探测。

非幂等性的接口，即便是相同参数，每次返回也会不一样。那就不建议做热点探测。比如下单，库存的查询，余额的查询。这样的接口如果一旦被升级成热点。那会影响业务界面的正确性和后续逻辑的判断错误。