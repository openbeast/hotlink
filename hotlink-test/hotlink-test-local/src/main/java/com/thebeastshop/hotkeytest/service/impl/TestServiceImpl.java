package com.thebeastshop.hotkeytest.service.impl;

import com.thebeastshop.hotkeytest.service.TestService;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class TestServiceImpl implements TestService {
    @Override
    public String getResult1(String name) {
        try{
            Thread.sleep(new Random().nextInt(10)*100);
            if(name.startsWith("_")){
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "hello," + name;
    }

    @Override
    public String getResult2(String name) {
        try{
            Thread.sleep(new Random().nextInt(10)*100);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "hello," + name;
    }
}
