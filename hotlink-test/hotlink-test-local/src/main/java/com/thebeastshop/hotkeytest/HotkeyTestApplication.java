package com.thebeastshop.hotkeytest;

import com.thebeastshop.hotkeytest.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@SpringBootApplication
@RestController
public class HotkeyTestApplication {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private TestService testService;

    public static void main(String[] args) {
        SpringApplication.run(HotkeyTestApplication.class, args);
    }

    @RequestMapping("/hi1")
    public String sayHello1(@RequestParam String name){
        return testService.getResult1(name);
    }

    @RequestMapping("/hi2")
    public String sayHello2(@RequestParam String name){
        return testService.getResult2(name);
    }
}
