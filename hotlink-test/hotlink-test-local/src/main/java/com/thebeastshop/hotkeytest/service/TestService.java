package com.thebeastshop.hotkeytest.service;

import com.thebeastshop.hotlink.annotation.Hotlink;

public interface TestService {

    @Hotlink
    String getResult1(String name);

    String getResult2(String name);
}
