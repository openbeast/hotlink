package com.thebeastshop.hotlink.util;

import cn.hutool.core.util.StrUtil;

import java.lang.reflect.Method;

public class MethodUtil {

    @SuppressWarnings("rawtypes")
    public static String paramStr(Method method){
        StringBuilder result = new StringBuilder();
        int count = 0;
        for (Class clazz : method.getParameterTypes()){
            if (++count == method.getParameterCount()){
                result.append(clazz.getSimpleName());
            }else{
                result.append(clazz.getSimpleName()).append(",");
            }
        }
        return result.toString();
    }

    @SuppressWarnings("rawtypes")
    public static String getInterfaceMethodStr(Class interfaceClass, Method method){
        return StrUtil.format("HOK_{}.{}({})",interfaceClass.getName(),method.getName(), MethodUtil.paramStr(method));
    }
}
