package com.thebeastshop.hotlink.test.dubbox.service;

import com.thebeastshop.hotlink.test.service.DemoService;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component("demoService")
public class DemoServiceImpl implements DemoService {
    @Override
    public String sayHello1(String name) {
        try{
            Thread.sleep(100 * new Random().nextInt(10));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "hello," + name;
    }

    @Override
    public String sayHello2(String name) {
        try{
            Thread.sleep(100 * new Random().nextInt(10));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "hello," + name;
    }
}
