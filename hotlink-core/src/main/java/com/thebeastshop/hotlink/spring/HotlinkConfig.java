package com.thebeastshop.hotlink.spring;

public class HotlinkConfig {

    private String appName;

    private String etcdUrl;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getEtcdUrl() {
        return etcdUrl;
    }

    public void setEtcdUrl(String etcdUrl) {
        this.etcdUrl = etcdUrl;
    }
}
