package com.thebeastshop.hotlink.test.dubbox.controller;

import com.thebeastshop.hotlink.test.service.DemoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
public class DemoController {

    @Resource
    private DemoService demoService;

    @RequestMapping("/hi1")
    public String sayHello1(@RequestParam String name){
        return demoService.sayHello1(name);
    }

    @RequestMapping("/hi2")
    public String sayHello2(@RequestParam String name){
        return demoService.sayHello2(name);
    }
}
